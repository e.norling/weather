var spreadsheet_key = '10Hye4qJcnqDmB87MODQM0y1VEUKH28ZopkQks_FeeEk';
var minute = 60*1000; // one minute in ms
var updateInterval = minute; // How often we should check for new data in the spreadsheet (every minute)
var best_height = 500;  // best height for graph display
var default_range = 7*24*60*60*1000; // 7 days
var line_chart, wind_gauge; // Line chart which while show any two data series; wind gauge shows current wind reading
var weather_data, wind_data;    // Data used to populate the line chart and wind gauge
var first_date, last_date, min_temp, min_temp_date, max_temp, max_temp_date;

function drawChart() {
    var min_field = document.getElementById("min_date_field");
    var min_date = new Date(min_field.value);
    var max_field = document.getElementById("max_date_field");
    var max_date = new Date(max_field.value);

    var y_axis_left = document.getElementById("y_axis_left");
    var y_axis_right = document.getElementById("y_axis_right");
    for (i = 0; i < y_axis_left.length; i++) {
        if (y_axis_left[i].checked) {
            left = i;
        }
        if (y_axis_right[i].checked) {
            right = i;
        }
    }


    var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

    var h = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;


    var width = (w > 1000) ? 0.6*w : w;
    var height = (h < best_height) ? h : best_height;

    // Get a view so that we only display the requested data
    var view = new google.visualization.DataView(weather_data);
    if (left === right) {
        series_to_show = {
            0: {targetAxisIndex: 0}
        }
        v_axes_to_show = {
            y: {
              0: {title: weather_data.getColumnLabel(left)}
            }
        }
        view.setColumns([0,left]);
    } else {
        series_to_show = {
            0: {targetAxisIndex: 0},
            1: {targetAxisIndex: 1}
        }
        vaxes_to_show = {
            0: {title: weather_data.getColumnLabel(left), viewWindowMode: 'pretty' },
            1: {title: weather_data.getColumnLabel(right), viewWindowMode: 'pretty' }
        }
        view.setColumns([0,left,right]);
    }

    line_chart.draw(view,
            {
                legend: { position: 'in' },
                title: 'Weather Station Data',
                width: width,
                height: height,
                aggregationTarget: 'category',
                hAxis: {
                  format: 'dd MMM',
                  viewWindow: {
                    min: min_date,
                    max: max_date,
                  },
                  gridlines: {
                    count: -1,
                    units: {
                      days: {format: ['MMM dd']},
                      hours: {format: ['HH:mm', 'ha']},
                    }
                  },
                  minorGridlines: {
                    units: {
                      hours: {format: ['hh:mm:ss a', 'ha']},
                      minutes: {format: ['HH:mm a Z', ':mm']}
                    }
                  }
                },
                vAxis: {
                    format: 'decimal',
                },
                animation: {

                    duration: 1000,
                    easing: 'in'
                },
                series: series_to_show,
                vAxes: vaxes_to_show,
            });
}

function loadData() {
    query = new google.visualization.Query('http://docs.google.com/spreadsheet/tq?key=' + spreadsheet_key + '&gid=0');

    // First get min and max dates in weather data
    query.setQuery('SELECT MIN(A), MAX(A)');
    query.send(function (response) {
        if (response.isError()) {
             console.log('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
             return;
        }

        data = response.getDataTable();
        first_date = data.getValue(0,0);
        last_date = data.getValue(0,1);

        // Use these to set the slider ranges
        var min_slider = document.getElementById("min_date_slider");
        var max_slider = document.getElementById("max_date_slider");
        var min_field = document.getElementById("min_date_field");
        var max_field = document.getElementById("max_date_field");
        var min_label = document.getElementById('min_date_label');
        var max_label = document.getElementById('max_date_label');

        min_label.innerHTML = first_date.toLocaleDateString();
        max_label.innerHTML = last_date.toLocaleDateString();

        min_slider.min = first_date.getTime();
        min_slider.max = last_date.getTime();
        min_slider.onchange = getMinSlider;

        max_slider.min = first_date.getTime();
        max_slider.max = last_date.getTime();
        max_slider.onchange = getMaxSlider;

       // Now get the data that will be used in the graph - default is
       // the last 10 days worth of data 
       var since, until;
       if (min_field.value === "" || min_field.value === undefined)
            since = new Date(last_date.getTime() - default_range);
        else
            since = new Date(parseInt(min_slider.value));
        if (max_field.value === "" || max_field.value === undefined)
            until = last_date;
        else
            until = new Date(parseInt(max_slider.value));
        
        if (since <  first_date) 
            since = first_date;
        query.setQuery('SELECT * WHERE A > datetime \''
                        + corrected_date_as_string(since)
                        + '\' AND A <= datetime \''
                        + corrected_date_as_string(until) + '\'');
        query.send(function (response) {
            if (response.isError()) {
                console.log('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }

            weather_data = response.getDataTable();


            min_field.value = since;
            min_slider.value = since.getTime();
            max_field.value = until;
            max_slider.value = until.getTime();
                
            chart_div = document.getElementById('chart_div');
            line_chart = new google.visualization.LineChart(chart_div);
            drawChart();
            update_stats();
        });
    });
}

function update_stats() {
    // Now get info for stats at bottom of the page

    // Get min and max temp (all records)
    range = weather_data.getColumnRange(1);
    min_temp = range.min;
    min_temp_date = weather_data.getValue(weather_data.getFilteredRows([{column: 1, value: min_temp}])[0], 0);
    max_temp = range.max;
    max_temp_date = weather_data.getValue(weather_data.getFilteredRows([{column: 1, value: max_temp}])[0], 0);
            
    // Get min and max temp (today)
    today = new Date(last_date.toDateString());
    view = new  google.visualization.DataView(weather_data);
    view.setRows(view.getFilteredRows([{column: 0, minValue: today}]));
    range = view.getColumnRange(1);
    min_today = range.min;
    min_today_date = view.getValue(view.getFilteredRows([{column: 1, value: min_today}])[0], 0);
    max_today = range.max;
    max_today_date = view.getValue(view.getFilteredRows([{column: 1, value: max_today}])[0], 0);

    var last_row = weather_data.getNumberOfRows()-1;
    latest_temp = weather_data.getValue(last_row,1);
    latest_wind = weather_data.getValue(last_row,3);
    wind_data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Wind (km/h)', latest_wind]
    ]);

    gauge_div =  document.getElementById('wind_gauge_div');
    wind_gauge = new google.visualization.Gauge(gauge_div);
    drawWindAndStats();
}

// Rather than grab all the data again, just add any that has appeared
// since last update (there should not be many)
function updateData() {
    get_new_data = (last_date == max_date_field.value);

    // First get last data in spreadsheet
    var query = new google.visualization.Query('http://docs.google.com/spreadsheet/tq?key=' + spreadsheet_key + '&gid=0');
    query.setQuery('SELECT MAX(A)');
    query.send(function (response) {
        if (response.isError()) {
             console.log('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
             return;
        }

        query.setQuery('SELECT * WHERE A > datetime \''
                + corrected_date_as_string(last_date) + '\'');
        data = response.getDataTable();
        last_date = data.getValue(0,0);
        end = last_date.getTime();

        // Update slider max values 
        var min_slider = document.getElementById("min_date_slider");
        var max_slider = document.getElementById("max_date_slider");
        var max_label = document.getElementById("max_date_label");
        min_slider.max = end;
        max_slider.max = end;
        max_label.innerHTML = last_date.toLocaleDateString();

        if (get_new_data) {
            query.send(function (response) {
                if (response.isError()) {
                    console.log('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }

                new_data = response.getDataTable();
                r = new_data.getNumberOfRows();
                for (i = 0; i < r; i++) {
                    weather_data.addRow([new_data.getValue(i,0),
                                            new_data.getValue(i,1),
                                            new_data.getValue(i,2),
                                            new_data.getValue(i,3),
                                            new_data.getValue(i,4),
                                            new_data.getValue(i,5)]);
                }

                // If we want the line chart to update with the new data,
                // expand to encompass that data and redraw it
                max_slider.value = end;
                var max_field = document.getElementById("max_date_field");
                max_field.value = last_date;
                drawChart();

            });
        }
        update_stats();
    });
}

// Called when user changes the minimum date slider
function getMinSlider() {
    // Get slider values
    var min_slider = document.getElementById("min_date_slider");
    var max_slider = document.getElementById("max_date_slider");

    // Don't let starting date exceed ending date
    var min = parseInt(min_slider.value);
    var max = parseInt(max_slider.value);
    if (min > max) {
        min = max;
        min_slider.value = min;
    }

    // Update both the text field AND the slider position
    var min_field = document.getElementById("min_date_field");
    min_field.value = new Date(min);
    loadData();
}

// Called when user changes the maximum date slider
function getMaxSlider() {
    // Get slider values
    var min_slider = document.getElementById("min_date_slider");
    var max_slider = document.getElementById("max_date_slider");

    // Don't let ending date preceed starting date
    var min = parseInt(min_slider.value);
    var max = parseInt(max_slider.value);
    var max_bound = parseInt(max_slider.max);
    if (max < min) {
        max = min;
        max_slider.value = max;
    }

    // Update both the text field AND the slider position
    var max_field = document.getElementById("max_date_field");
    max_field.value = new Date(max);
    loadData();
}

// Called when user changes the minimum date field
function minDateChange() {
    var min_field = document.getElementById("min_date_field");
    var min_slider = document.getElementById("min_date_slider");
    date = new Date(min_field.value);
    min = date.getTime();
    if (isNaN(date)) {
        alert("Wrong date format. Valid formats are:\n"
            + "Short: mm/dd/yyyy hh:mm (Sorry yes, American style date!)\n"
            + "Long: dd Mmm yyy hh:mm\n"
            + "ISO: yyyy-mm-dd hh:mm\n"
            + "Use 24 hour format for time.\n"
            + "For any format, you can omit time (which will default to 00:00).\n"
            + "For ISO format, you can stop at any point,\n"
            + "e.g. \"yyyy-mm\" will be interpreted as \"yyyy-mm-01 00:00\""
        );
        getMinSlider();
    } else {
        // Limit date to the earliest data point we have
        var min_bound = new Date(min_slider.min);
        if (date < min_bound) {
            date = min_bound;
        }
        // Don't allow min date to be greater than the maximum date
        var max_slider = document.getElementById("max_date_slider");
        var max = parseInt(max_slider.value);
        if (min > max) {
            min = max;
            date = new Date(min);
        }
        // Update both the text field AND the slider position
        min_field.value = date;
        min_slider.value = min;
        loadData();
    }
}

// Called when user changes the minimum date field
function maxDateChange() {
    var max_field = document.getElementById("max_date_field");
    var max_slider = document.getElementById("max_date_slider");
    date = new Date(max_field.value);
    max = date.getTime();
    if (isNaN(date)) {
        alert("Wrong date format. Valid formats are:\n"
            + "Short: mm/dd/yyyy hh:mm (Sorry yes, American style date!)\n"
            + "Long: dd Mmm yyy hh:mm\n"
            + "ISO: yyyy-mm-dd hh:mm\n"
            + "Use 24 hour format for time.\n"
            + "For any format, you can omit time (which will default to 00:00).\n"
            + "For ISO format, you can stop at any point,\n"
            + "e.g. \"yyyy-mm\" will be interpreted as \"yyyy-mm-01 00:00\""
        );
        getMaxSlider();
    } else {
        // Limit date to the latest data point we have
        var max_bound = parseInt(max_slider.max);
        if (max > max_bound) {
            date = new Date(max_bound);
        }
        // Don't allow max date to be less than minimum date
        var min_slider = document.getElementById("min_date_slider");
        var min = parseInt(min_slider.value);
        if (max < min) {
            max = min;
            date = new Date(max);
        }
        // Update both the text field AND the slider position
        max_field.value = date;
        max_slider.value = max;
        loadData();
    }
}

// Updates the wind gauge and temperature stats on the web page
function drawWindAndStats() {
    var options = {
      width: 400, height: 120,
      max: 50,
      redFrom: 40, redTo: 50,
      yellowFrom:30, yellowTo: 40,
      minorTicks: 5
    };

    wind_gauge.draw(wind_data, options);

    stats_txt = 'Most recent temperature is <strong>' + latest_temp
            + 'C</strong> (recorded ' + pretty_date_string(last_date)
            + ')<br>' + '<strong>Today\'s temperatures</strong>'
            + '<br>Min: ' + min_today + 'C at ' + min_today_date.toLocaleTimeString()
            + '<br>Max: ' + max_today + 'C at ' + max_today_date.toLocaleTimeString()
            + '<br>' + '<strong>Temperature range in period displayed:</strong>'
            + '<br>' + 'Minimum temperature ' + min_temp
            + 'C on ' + pretty_date_string(min_temp_date)
            + '<br>' + 'Maximum temperature ' + max_temp
            + 'C on ' + pretty_date_string(max_temp_date)
            + '<br>';
    var displayElement = document.getElementById("stats");
    displayElement.innerHTML = stats_txt;
}

// Return the date as a string, formatted for query
function corrected_date_as_string(date) {
    corrected = new Date(date.getTime() - date.getTimezoneOffset()*minute);
    return (corrected.toISOString().replace("T", " ")).replace("Z", "");
}

function pretty_date_string(date) {
    return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
}

google.charts.load('current', {packages: ['corechart', 'gauge']});
google.charts.setOnLoadCallback(loadData);
interval = setInterval(updateData, updateInterval);
