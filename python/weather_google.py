import threading
from time import sleep
from gpiozero import DigitalInputDevice
from w1thermsensor import W1ThermSensor
from envirophat import weather, light
import json # used to parse config.json
import datetime # log and plot current time
import math

# libraries
import sys
import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Oauth JSON File
GDOCS_OAUTH_JSON       = 'Weather Station-aef07e27db03.json'

# Google Docs spreadsheet name.
GDOCS_SPREADSHEET_NAME = 'Sensors'

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']


def login_open_sheet(oauth_key_file, spreadsheet):
    """Connect to Google Docs spreadsheet and return the first worksheet."""
    timeout = 2
    while True:
        try:
            json_key = json.load(open(oauth_key_file))
            credentials = ServiceAccountCredentials.from_json_keyfile_name('Weather Station-aef07e27db03.json', scope)
            gc = gspread.authorize(credentials)
            worksheet = gc.open(spreadsheet).sheet1
            return worksheet
        except Exception as ex:
            print ('Unable to login and get spreadsheet.  Check OAuth credentials, spreadsheet name, and make sure spreadsheet is shared to the client_email address in the OAuth .json file!')
            print ('Google sheet login failed with error:', ex)
            print ('Retrying in', timeout, 'seconds')
            sleep(timeout)
            timeout = timeout*2

unit = 'hPa' # Pressure unit, can be either hPa (hectopascals) or Pa (pascals)

temp_C = 0
rainfall = 0
wind_count = 0
bucket_count = 0
radius_cm = 9.0
interval = 60*5     # 5 minute intervals
now = datetime.datetime.now()
ADJUSTMENT = 1.18
CM_IN_A_KM = 100000.0
SECS_IN_AN_HOUR = 3600
BUCKET_SIZE = 0.2794

sensor = W1ThermSensor()
wind_speed_sensor = DigitalInputDevice(17, pull_up=True)
rain_sensor = DigitalInputDevice(27, pull_up=True)


def wind(time_sec):
    global wind_count
    circumference_cm = (2 * math.pi) * radius_cm
    rotations = wind_count / 2.0
    wind_count = 0

    dist_km = (circumference_cm * rotations) / CM_IN_A_KM

    km_per_sec = dist_km / time_sec
    km_per_hour = km_per_sec * SECS_IN_AN_HOUR

    return km_per_hour * ADJUSTMENT
 

def spin():
    global wind_count
    wind_count = wind_count + 1
 
def rain():
    global rainfall 
    global bucket_count
    
    bucket_count = bucket_count + 1
    rainfall = (bucket_count * BUCKET_SIZE - BUCKET_SIZE)
    print("Rainfall is currently "+str(rainfall)+"mm \n")


windspeed = threading.Thread(name='wind', target=wind(interval))
raindata = threading.Thread(name='rain', target=rain)

windspeed.start()
raindata.start()

wind_speed_sensor.when_activated = spin
rain_sensor.when_activated = rain

print ('Logging sensor measurements to {0} every {1} seconds.'.format(GDOCS_SPREADSHEET_NAME, interval))
print ('Press Ctrl-C to quit.')
worksheet = None
while True:
    # Login if necessary.
    if worksheet is None:
        worksheet = login_open_sheet(GDOCS_OAUTH_JSON, GDOCS_SPREADSHEET_NAME)

    # Attempt to get sensor readings.
    now = datetime.datetime.now()
    midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
    seconds = (now - midnight).seconds
    temp_C = sensor.get_temperature()
    p = weather.pressure(unit=unit)
    wind_speed = round(wind(interval),2)
    white = light.light()

    try:
        worksheet.append_row((str(now), temp_C, p, wind_speed, rainfall, white), value_input_option='USER_ENTERED')
    except:
        # Error appending data, most likely because credentials are stale.
        # Null out the worksheet so a login is performed at the top of the loop.
        print ('Append error, logging in again')
        worksheet = None
        continue

    # Wait <interval> seconds before continuing
    print ('Wrote a row to {0}'.format(GDOCS_SPREADSHEET_NAME))
    sleep(interval)
